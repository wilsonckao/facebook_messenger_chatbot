var restify = require('restify');
var builder = require('botbuilder');
const express = require('express');
const bodyParser = require('body-parser')
const app = express();
const BitlyClient = require('bitly');
const bitly = BitlyClient('76260c787076a732bca196f5147640169f3ff012');
var MongoClient = require('mongodb').MongoClient;
var Facebook = require('facebook-node-sdk');
const token = "EAACxwSDYYtMBAGA2AU5N2coHnlYL9ZBhZAUWem4kjHGNhcMhT8iRcTu9F9GoOfkojY6mzyS1SZBJ8LqZBkM5P4xz6EwYmHAB7TxlYSTFFKdDRqDJuFNxKB5ZA6FHZBlzBckq9g3ALdTUw8lULxGDEc9vsrwveb4dcww4t7vgUJmOIupLdg67S5WPhScw73zY8ZD"
var facebook = new Facebook({ appID: '195443037856467', secret: 'ddb5c777da9eda4f80c5891f98588293' }).setAccessToken(token);
var url = "mongodb://test:test@ds239638.mlab.com:39638/penta_ga_db";
var fbId;
var accessTokenFlag = "";
MongoClient.connect(url, function (err, db) {
	console.log("database is connected");
	db.close();
})
var ObjectId = require('mongodb').ObjectID;
//global.projecturl = "https://ga-dev-bot.herokuapp.com/"; //for live
global.projecturl = "http://localhost:3000/"; //for local
// Create bot and add dialogs
var connector = new builder.ChatConnector({
    appId: "",
    appPassword: ""
});

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
	extended: false
}))

// parse application/json
app.use(bodyParser.json())

app.use('/static', express.static('assets'));

app.use(express.static("public"));

// for facebook verification
app.get('/webhook/', function (req, res) {
	console.log("webhook token ===>", req.query['hub.verify_token']);
	if (req.query['hub.verify_token'] === 'crowdbotics') {
		res.send(req.query['hub.challenge'])
	} else {
		res.send('Error, wrong token')
	}
})
global.reportTimeLine = "";
global.reportName = "";
// to post data
app.post('/webhook/', function (req, res) {
	let messaging_events = req.body.entry[0].messaging


	for (let i = 0; i < messaging_events.length; i++) {
		let event = req.body.entry[0].messaging[i]
		let sender = event.sender.id
		fbId = sender;

		var user_name = "there";
		let url = "https://graph.facebook.com/v2.6/"+sender+"?fields=first_name,last_name,profile_pic&access_token="+token;
		facebook.api(url, function(err, data){
			if(err){
				console.error(err);
				
			}
			else{
				//Do some stuff with the data object
				console.log("USER DATA ~~~~~~~~~ ", data)
				user_name = data.first_name;
			}
		});
		console.log("Event in webhook ==>", event);
		if (event.message) {
			if(event.message.quick_reply){
				let text = event.message.quick_reply.payload;
				if(text == "auto_alert_yes"){
						saveAutoAlert(sender);
				}
				else if(text == "auto_alert_no"){
					sendTextMessage(sender, "Ok, no problem! Feel free to come back anytime!", function(){
						basicQuickReplies(sender);
					});
					
				}
				else if(text == "help"){
					sendTextMessage(sender, "Oh, I see. You can know more about me here. http://ai.pentabots.com/Bot/about", function(){
						basicQuickReplies(sender);
					});
				}
				else if(text == "website_status"){
					reportTimeLine = "";
					reportName = "";
					websiteStatus(sender);
				}
				else if(text == "go_back"){
					reportTimeLine = "";
					reportName = "";
					goBack(sender);
				}
				else if(text == "traffic_report" || text == "source_report" || text == "bounce_rate_report" || text == "page_report" || text == "new_users_report" || text == "transactions_report" || text == "page_speed_report"){
					reportName = event.message.text; //type of report
					console.log("reportTimeLine --->", reportTimeLine);
					if(reportTimeLine != "" && reportTimeLine != undefined){
						sendTextMessage(sender, "Ok. All set. Report of "+reportName+" for "+reportTimeLine+" timeline is here.", function(){
							getWebsiteReport(sender);
						});
					}
					else{
						request({
							url: 'https://graph.facebook.com/v2.6/me/messages',
							qs: {
								access_token: token
							},
							method: 'POST',
							json: {
								recipient: {
									id: sender
								},
								message: {
									"text": "Ok. You have selected "+event.message.text+". How often do you want to get notified?",
									"quick_replies":[
										{
										  "content_type":"text",
										  "title":"Daily",
										  "payload":"daily"
										},
										{
											"content_type":"text",
											"title":"Weekly",
											"payload":"weekly"
										},
										{
											"content_type":"text",
											"title":"Monthly",
											"payload":"monthly"
										}
									  ]
								},
							}
						}, function (error, response, body) {
							if (error) {
								console.log('Error sending messages: ', error)
							} else if (response.body.error) {
								console.log('Error: ', response.body.error)
							}
						})
					}
						
				}
				else if(text == "daily" || text == "weekly" || text == "monthly"){
					reportTimeLine = text;
					sendTextMessage(sender, "Ok. All set. Report of "+reportName+" for "+reportTimeLine+" timeline is here.", function(){
						getWebsiteReport(sender);
					});
				}
			}
			else{
				let text = event.message.text.toLowerCase();
				var shortUrl = "";
				console.log("TEXT IS ===>", text);
				if (text == 'hi' || text == 'get started' || text == 'login') {
					bitly.shorten(projecturl+"googlesignin?fbID=" + sender)
						.then(function(result) {
						console.log("THE SHORTEN URL IS ---->",result.data.url);
						shortUrl = result.data.url; 
						sendTextMessage(sender, "Hello "+user_name+"! I can help you understand your Google Analytics data. For that click on below link to Sign in with Google. " + shortUrl + "  .");
						})
						.catch(function(error) { 
						console.error(error);
						sendTextMessage(sender, "Something went wrong.");
						});
					
				}
				
				else if(text == "help"){
					sendTextMessage(sender, "Oh, I see. You can know more about me here.http://ai.pentabots.com/Bot/about", function(){
						basicQuickReplies(sender);
					});
				}
				
				else {
					//sendTextMessage(sender, "Text received, echo: " + text.substring(0, 200))
					sendTextMessage(sender, "Sorry, I do not understand this right now.", function(){
						basicQuickReplies(sender);
					})
				}
			}

		} else if (event.postback) {
			let text = event.postback.payload.toLowerCase();
			console.log("in postback--->", text);
			if (text == "login" || text == 'get_started_payload' || text == 'about us' || text == 'help' || text == "get started" ) {
				sendTextMessage(sender, "Hello "+user_name+"! I can help you understand your Google Analytics data. For that click on below link to Sign in with Google. "+projecturl+"googlesignin?fbID=" + sender);
			}
			else if(text == "go_back"){
				reportTimeLine = "";
				reportName = "";
				goBack(sender);
			}
			else if(text == "website_status"){
				console.log("hows my website ------");
				reportTimeLine = "";
				reportName = "";
				websiteStatus(sender);
			}
			else if(text == "get_traffic"){
				getTraffic(sender);
			}
			else if(text == "get_source"){
				getSource(sender);
			}
			else if(text == "bounce_rate"){
				getBouceRate(sender);
			}
			else if(text == "get_popular_page"){
				getPopularPage(sender);
			}
			else if(text == "new_users"){
				getNewUsers(sender);
			}
			else if(text == "get_transactions"){
				getTransactions(sender);
			}
			else if(text == "page_speed"){
				getPageSpeed(sender);
			}
			else if(text == "website_report"){
				getWebsiteReport(sender);
			}

			else{
				sendTextMessage(sender, "There is something I dont understand.");
				//send quick replies of help
				basicQuickReplies(sender);
			}
			
			
			break;
		}
	}
	res.sendStatus(200)
})

var bot = new builder.UniversalBot(connector); 

bot.dialog('/', function (session) {
    console.log(session.message.text);
    userMessage = session.message.text;
    if(userMessage == "get_started_payload"){
        session.replaceDialog('gettingStarted');
    }

    
});

bot.dialog('gettingStarted', function(session){
    session.send('Hello There!I can help you understand your Google Analytics data. For that click on below link to Sign in with Google. '+ projecturl+'googlesignin?fbID=123  .');
});

bot.dialog('getWebsiteReport', function(){
	session.semd("hero card will be displayed here!");
}).triggerAction({ matches: 'hero' });

app.get("/googlesignin", function (req, res) {
	console.log("****************************1", req.query.fbID);
	console.log("NEED TO FIND QUERY STRING ==>", req.query);
	REDIRECT_URL = projecturl+'googlesuccess';
	oauth2Client = new OAuth2Client(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);
	var url = getUrl(req.query.fbID);
	console.log("REGENRATED URL IS ==>", url);
	res.redirect(url);
})

// Setup Restify Server
var server = restify.createServer();
server.post('/api/messages', connector.listen());
server.listen(process.env.port || 3978, function () {
    console.log('%s listening to %s', server.name, server.url); 
});

// Setup Restify Server
app.set('port', (process.env.PORT || 3000))